import React, { Component } from "react";
import "./App.css";
import Dashboard from "./components/dashboard";

class App extends Component {
  constructor() {
    super();
    this.state = {
      substring: "",
    };
  }

  componentDidMount = () => {};
  render() {
    return (
      <div className="App">
        <Dashboard />
      </div>
    );
  }
}

export default App;
