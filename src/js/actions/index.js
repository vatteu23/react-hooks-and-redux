export const FETCH_SHOWS_PENDING = "FETCH_ADS_PENDING";
export const FETCH_SHOWS_SUCCESS = "FETCH_ADS_SUCCESS";
export const FETCH_SHOWS_ERROR = "FETCH_ADS_ERROR";

/* SHOWS ****************************/

export function fetchShowsPending() {
  return {
    type: FETCH_SHOWS_PENDING,
    pending: true,
  };
}

export function fetchShowsSuccess(shows) {
  return {
    type: FETCH_SHOWS_SUCCESS,
    shows: shows,
    pending: false,
  };
}

export function fetchShowsError(error) {
  return {
    type: FETCH_SHOWS_ERROR,
    pending: false,
    error: error,
  };
}
