import {
  fetchShowsError,
  fetchShowsPending,
  fetchShowsSuccess,
} from "./../actions/index";

function fetchShows(data) {
  return (dispatch) => {
    dispatch(fetchShowsPending());

    fetch("http://api.tvmaze.com/search/shows?q=" + data)
      .then((res) => res.json())
      .then((result) => dispatch(fetchShowsSuccess(result)))
      .catch((error) => dispatch(fetchShowsError(error)));
  };
}

export default fetchShows;
