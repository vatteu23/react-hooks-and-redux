import {
  FETCH_SHOWS_SUCCESS,
  FETCH_SHOWS_PENDING,
  FETCH_SHOWS_ERROR,
} from "../actions";

const initialState = {
  pending: true,
  shows: [],
  error: false,
};
export default function showsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SHOWS_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_SHOWS_SUCCESS: {
      return {
        ...state,
        pending: false,
        shows: action.shows,
      };
    }
    case FETCH_SHOWS_ERROR: {
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    }

    default:
      return state;
  }
}

export const getShows = (state) => state.shows;
export const getShowsPending = (state) => state.pending;
export const getShowsError = (state) => state.error;
