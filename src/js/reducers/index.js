import { combineReducers } from "redux";
import shows from "./showsReducer";

export default combineReducers({
  shows,
});
