import React, { Component, useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import fetchShows from "../js/actioncreators/getShows";
import {
  getShows,
  getShowsError,
  getShowsPending,
} from "../js/reducers/showsReducer";

const mapStateToProps = (state) => ({
  error: getShowsError(state.shows),
  shows: getShows(state.shows),
  pending: getShowsPending(state.shows),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchShows: fetchShows,
    },
    dispatch
  );

// class Dashboard extends Component {
//   constructor() {
//     super();
//     this.state = {
//       substring: "",
//     };
//   }

//   componentDidMount = () => {};

//   loadShows = (result) => {
//     let shows = [],
//       sortedShows = [],
//       Output = "",
//       showsWithoutImage = 0;
//     if (result.length > 0) {
//       result.map((res) => {
//         res.show.image
//           ? (showsWithoutImage = showsWithoutImage)
//           : (showsWithoutImage += 1);
//         shows.push(res.show);
//       });

//       //Sort shows by name
//       sortedShows = shows.sort((a, b) => {
//         return a.name > b.name ? 1 : -1;
//       });
//       //Set State to display list/ As we are skipping shows without images, I am letting user know
//       //if we have entire list without any shows (Example substring: ff)
//       showsWithoutImage !== sortedShows.length
//         ? this.setState({
//             sortedShows: sortedShows,
//             errorMessage: null,
//           })
//         : this.setState({
//             sortedShows: null,
//             errorMessage:
//               "We have " + showsWithoutImage + " show(s) without image.",
//           });

//       //Formatting output
//       sortedShows.map((show, index) => {
//         index === sortedShows.length - 1
//           ? (Output += show.name)
//           : (Output += show.name + " ");
//       });

//       //Task 1 Output
//       console.log(Output);
//     } else {
//       //If no results found, letting user know
//       this.setState({
//         sortedShows: null,
//         errorMessage: "No results found.",
//       });
//     }
//   };

//   getTVShowNames = (substr) => {
//     if (substr !== "") {
//       this.props.fetchShows(substr);
//     }
//     // substr !== ""
//     //   ? //Get Shows
//     //     ""
//     //   : this.setState({
//     //       sortedShows: null,
//     //       errorMessage: "Please enter a show name.",
//     //     });
//   };

//   render() {
//     console.log(this.props);
//     return (
//       <div className="App">
//         <div className="search-component">
//           <input
//             type="text"
//             className="search-field"
//             placeholder="Enter Show Name"
//             onChange={(e) => {
//               this.setState({ substring: e.target.value });
//             }}
//             onKeyDown={(e) => {
//               if (e.key === "Enter") this.getTVShowNames(this.state.substring);
//             }}
//             value={this.state.substring}
//           />
//           <button
//             className="btn"
//             onClick={() => {
//               this.getTVShowNames(this.state.substring);
//             }}
//           >
//             GET SHOWS
//           </button>
//         </div>
//         <div className="results-component">
//           {this.state.sortedShows
//             ? this.state.sortedShows.map((show, index) => {
//                 return (
//                   <React.Fragment key={index}>
//                     {show.image ? (
//                       <div className="show-card">
//                         <img alt={show.name} src={show.image.medium} />
//                         <h4>{show.name}</h4>
//                       </div>
//                     ) : null}
//                   </React.Fragment>
//                 );
//               })
//             : null}

//           {this.state.errorMessage ? (
//             <div>{this.state.errorMessage}</div>
//           ) : null}
//         </div>
//       </div>
//     );
//   }
// }

function Dashboard(props) {
  const [searchTerm, setSearchTerm] = useState("");
  const [sortedShows, setSortedShows] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (props.shows && searchTerm != "") {
      loadShows(props.shows);
    }
  }, [props.shows]);

  const getTVShowNames = (substr) => {
    if (substr !== "") {
      props.fetchShows(substr);
    }
  };

  const loadShows = (result) => {
    let shows = [],
      sortedShows = [],
      Output = "",
      showsWithoutImage = 0;
    if (result.length > 0) {
      result.map((res) => {
        res.show.image
          ? (showsWithoutImage = showsWithoutImage)
          : (showsWithoutImage += 1);
        shows.push(res.show);
      });

      //Sort shows by name
      sortedShows = shows.sort((a, b) => {
        return a.name > b.name ? 1 : -1;
      });
      //Set State to display list/ As we are skipping shows without images, I am letting user know
      //if we have entire list without any shows (Example substring: ff)
      if (showsWithoutImage !== sortedShows.length) {
        setSortedShows(sortedShows);
        setErrorMessage(null);
      } else {
        setSortedShows([]);
        setErrorMessage(
          "We have " + showsWithoutImage + " show(s) without image."
        );
      }
      //   //Formatting output
      //   sortedShows.map((show, index) => {
      //     index === sortedShows.length - 1
      //       ? (Output += show.name)
      //       : (Output += show.name + " ");
      //   });

      //   //Task 1 Output
      //   console.log(Output);
    } else {
      //If no results found, letting user know
      setSortedShows([]);
      setErrorMessage("No results found.");
    }
  };

  return (
    <div className="App">
      <div className="App">
        <div className="search-component">
          <input
            type="text"
            className="search-field"
            placeholder="Enter Show Name"
            onChange={(e) => {
              setSearchTerm(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") getTVShowNames(searchTerm);
            }}
            value={searchTerm}
          />
          <button
            className="btn"
            onClick={() => {
              getTVShowNames(searchTerm);
            }}
          >
            GET SHOWS
          </button>
        </div>
        <div className="results-component">
          {sortedShows.length > 0
            ? sortedShows.map((show, index) => {
                return (
                  <React.Fragment key={index}>
                    {show.image ? (
                      <div className="show-card">
                        <img alt={show.name} src={show.image.medium} />
                        <h4>{show.name}</h4>
                      </div>
                    ) : null}
                  </React.Fragment>
                );
              })
            : null}

          {errorMessage ? <div>{errorMessage}</div> : null}
        </div>
      </div>
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
