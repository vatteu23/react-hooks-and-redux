#Functionality

- Search text box to pass substring to the API (Works with Enter/Button Click)
- Displays show names in console.(Result is based on the input)
- Displays list of shows with images below text box (Only shows with images are 
  displayed)
- If a result set consists of all the shows without image, an error is displayed
  (Example Substring: ff)